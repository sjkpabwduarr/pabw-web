<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    protected$primaryKey = 'id';
    protected $fillable = [
    	'id',
		'homestay_id',
		'ulasan',
    	'tanggal',    	
        'user_id',
        'rate',
        ];
    	public $timestamps = false;
}
