<?php

namespace App\Http\Controllers;

use App\House;
use File;
use Illuminate\Http\Request;
//pake DB
use Illuminate\Support\Facades\DB;



class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houses = House::all();
        return view('houses.index',compact('houses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userid = auth()->user()->id;
        $homestay = House::where('user_id','like',"%".$userid."%")->paginate();
        $houses = House::all();
        return view('houses.create',compact('houses','homestay'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id =DB::table('houses')->max('id') +1;
        $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=strval($id).'-'.$file->getClientOriginalName();
                $file->move('image',$name);
                $images[]=$name;
            }
        }        
         $request -> validate([ 
            'nama' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
            'user_id' => 'required',
            'harga' => 'required',
            'kamar' => 'required',
            'image' =>'required'
        ]);
         
         DB::table('houses')->insert([
             'id'=>$id,
             'nama'=>$request->nama,
             'alamat'=>$request->alamat,
             'kota' =>$request->kota,
             'provinsi' => $request->provinsi,
             'deskripsi' => $request->deskripsi,
             'status' => $request->status,
             'user_id' => $request->user_id,
             'rating'=> 5,
             'reviews'=> 0,
             'kamar' => $request->kamar,
             'harga' => $request->harga,
             'image'=>  implode("|",$images),             
         ]);
        return redirect('/projeklaravel/public/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(House $house)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    //edit data via
    public function edit(Request $request,$id)
    {       
        $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=strval($id).'-'.$file->getClientOriginalName();
                $file->move('image',$name);
                $images[]=$name;
            }
        }
        
        if($images!=null){
        $imagestemp = DB::table('houses')->where('id',$id)->get();
        foreach ($imagestemp as $fileString) {            
        $arrayOfFiles = explode('|',$fileString->image);
          }
        foreach ($arrayOfFiles as $filename){
        File::delete('image/'.$filename);
        }        
        DB::table('houses')->where('id',$id)->update([
            'image'=>  implode("|",$images)
        ]);
        }        
         $request -> validate([ 
            'nama' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
            'harga' => 'required',
            'kamar' => 'required'        
        ]);

         DB::table('houses')->where('id',$id)->update([             
             'nama'=>$request->nama,
             'alamat'=>$request->alamat,
             'kota' =>$request->kota,
             'provinsi' => $request->provinsi,
             'deskripsi' => $request->deskripsi,
             'status' => $request->status,
             'harga' => $request->harga,
             'kamar' => $request->kamar,                                     
         ]);

            return redirect('projeklaravel/public/create');
    }
    
    public function update(Request $request, House $house)
    {
        //
    }

    public function destroy($id)
    {
        $images = DB::table('houses')->where('id',$id)->get();
        foreach ($images as $fileString) {            
        $arrayOfFiles = explode('|',$fileString->image);
          }
        foreach ($arrayOfFiles as $filename){
        File::delete('image/'.$filename);
        }        
        DB::table('houses')->where('id',$id)->delete();
        DB::table('ulasan')->where('homestay_id',$id)->delete();
        return redirect('projeklaravel/public/create');
    }
    

    public function cari(Request $request)
	{
		// menangkap data pencarian
        $cari = $request->cari;
        
    		// mengambil data dari table homestay sesuai pencarian data
		$homestay = DB::table('houses')
		->where('kota','like',"%".$cari."%")
        ->paginate();
        
    		// mengirim data homestay ke view index
        return view('search',['homestay' => $homestay]);
    }

    public function indexsearch()
	{
    		// mengambil data dari table homestay
        $homestay = DB::table('houses')->paginate(10);
        
 
    		// mengirim data homestay ke view index
		return view('search',['homestay' => $homestay]);
 
    }

    //mengambil data homestay berdasarkan user

    public function homestayku()
    {
        $userid = auth()->user()->id;
        $homestay = DB::table('houses')
		->where('user_id','like',"%".$userid."%")
		->paginate();
        $houses = House::all();
        
            // mengirim data homestay ke view index
        $orders=array();        
        $orders = DB::table('orders')->where('pemilik_id',$userid)->orderBy('date')->get();      
        return view('houses.create',['homestay' => $homestay , 'orderan' => $orders]);
    }


    public function jumpedit($id)
    {
    $homestay = DB::table('houses')->find($id);
    return view('houses.edit',['homestay'=> $homestay]);
    }

    public function showpage($id)
    {
        $images = DB::table('houses')->where('id',$id)->get();
        foreach ($images as $fileString) {            
            $arrayOfFiles = explode('|',$fileString->image);
          }
        $homestay = DB::table('houses')->find($id);
        $ulasan = DB::table('ulasan')->where('homestay_id',$id)->get();
        return view('houses.showpage',['homestay' => $homestay,'images'=> $arrayOfFiles,'ulasan'=>$ulasan]);
    }

    public function order(Request $request,$id)
    {
        DB::table('houses')->where('id',$id)->update(['status'=>'booked']);
        $finaldate1 = date('Y-m-d',strtotime($request->date . "+$request->days days")); 
        $orderid =DB::table('orders')->max('id') +1;
        $house=DB::table('houses')->find($id);
        $total=$house->harga*$request->days;

        $everytanggal=DB::table('orders')->where('homestay_id',$id)->where('status','booking')->get();
        $pass = true;
        foreach ($everytanggal as $bookeddate){
            $diff = date_diff(new \DateTime($request->date),new \DateTime($bookeddate->date));
            $diff1 = date_diff(new \DateTime($request->date),new \DateTime($bookeddate->finaldate));
            $diff2 = date_diff(new \DateTime($finaldate1),new \DateTime($bookeddate->date));
            $diff3 = date_diff(new \DateTime($finaldate1),new \DateTime($bookeddate->finaldate));
            $diff->format("%R%a");
            if( (((int)$diff->format("%R%a")  <= 0) AND ((int)$diff1->format("%R%a") >= 0) ) OR (( (int)$diff2->format("%R%a")  <= 0) AND ((int)$diff3->format("%R%a") >= 0)) OR ( ((int)$diff->format("%R%a")  >= 0) AND ((int)$diff3->format("%R%a")  <= 0) ) ){
                $pass=false;
            }
        }
        if($pass==false){
            return view("houses.booked",['homestay'=> $everytanggal,'id'=>$id]);
        }
        if($pass==true){
            DB::table('orders')->insert(['id'=>$orderid,'homestay_id'=>$id,'date'=>$request->date,'days'=>"+$request->days days",'finaldate'=>$finaldate1,'user_id'=>$request->user_id,'total'=>$total,'status'=>'booking','pemilik_id'=>$house->user_id,'ulas'=>false]);
            return redirect("/projeklaravel/Myorder/");
        }                    
    }  
    
    

    public function createulas(Request $request,$id)
    {   $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=strval($id).'-'.$file->getClientOriginalName();
                $file->move('image',$name);
                $images[]=$name;
            }
        }
        $homestay_id = DB::table('orders')->find($id)->homestay_id;
        DB::table('ulasan')->insert(['id'=> DB::table('ulasan')->max('id')+1,'homestay_id'=>$homestay_id,'ulasan'=>$request->ulasan,'rate'=>$request->rate,'order_id'=>$id,'tanggal'=> date("Y-m-d"),'image'=> implode("|",$images),'user_id'=>auth()->user()->id ]);
        $homestay = DB::table('houses')->find($homestay_id);
        DB::table('houses')->where('id',$homestay_id)->update(['rating'=>($homestay->rating+$request->rate)/$homestay->reviews+1,'reviews'=>$homestay->reviews+1]);
        DB::table('orders')->where('id',$id)->update(['ulas'=>true]);
        return redirect("/projeklaravel/$homestay_id/show");    
    }

    public function jumpulas ($id){
        $orders=DB::table('orders')->find($id);
        return view('houses.createulasan',['orders'=>$orders]);
    }    

    public function cancelorder($homestay_id){
        $userid=auth()->user()->id;
        DB::table('orders')->where('homestay_id',$homestay_id)->where('user_id',$userid)->where('status','booking')->update(['status' => 'Cancelled']);
        return redirect("/projeklaravel/$homestay_id/show");
    } 

    public function vieworder()
    {
        $id=auth()->user()->id;
        $order=DB::table('orders')->where('user_id',$id)->get();
        $namahomestay=DB::table('houses')->find($id);        
        return view('houses.myorder',['order' => $order , 'nama' => $namahomestay]);
    }
}
