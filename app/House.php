<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
	protected$primaryKey = 'id';
    protected $fillable = [
    	'id',
		'nama',
		'user_id',
    	'alamat',
    	'kota',
    	'provinsi',
    	'deskripsi',
		'status',
		'harga',
		'image'
    	];
    	public $timestamps = false;
}
