
@extends('layouts.postlogin')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Cari</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/Article-Cards.css">
    <link rel="stylesheet" href="/css/Card-Group1-Shadow.css">
    <link rel="stylesheet" href="/css/styles.css">

@section('content')

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script> 
                    <h1 style="text-align: center;"> List Homestay </h1> 
                    <center>   
                    <body>
    <section>
        <div class="container">
            <div class="row">

                @if($homestay!=null)
                @foreach($homestay as $p)
                <a  style="color:black; text-decoration:none" href="{{ ('/projeklaravel/'. $p -> id . '/show')}}">  
                <div class="card mb-4 box-shadow">
                <img style='height: 100%; width: 100%;  height: 200px; width: 300px; object-fit: contain' src="/image/<?php
                $images = explode('|',$p->image);                                       
                $image = max($images);
                echo $image                                  
                ?>"/>
                <div class="card-body">
                <h3 class="card-title">{{$p->nama}}</h3>
                <h4 class="text-muted card-subtitle mb-2">Rp. {{$p->harga}} / hari</h4>
                <h5 class="text-muted card-subtitle mb-2">Kota {{$p->kota}}</h5>
                
                @if($p->reviews!=0)
                <h6 class="text-muted card-subtitle mb-2">Rating : {{$p->rating}} / {{$p->reviews}} reviews</h6>
                @endif
                @if($p->reviews==0)
                
                <h6 class="text-muted card-subtitle mb-2">Belum ada reviews</h6>
                @endif
                <p class="card-text">Kamar : {{$p -> kamar}}</p>
                </div>                  
                 </div>
                 @endforeach
                @endif
                </div>               
         </center>      
     </div>
 </div>
                    
                   
                     
                

    </section>
    <script src="/js/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>               
                                   
@endsection