<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<a href="/projeklaravel/public/create">Tambah Data</a>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<thead>
			<table class='table table-bordered'>
			<tr>
				<th>No</th>
				<th>Nama Homestay</th>
				<th>Alamat</th>
				<th>Kota</th>
				<th>Provinsi</th>
				<th>Deskripsi</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($houses as $h)
			<tr>
				<td>{{$loop -> iteration}}</td>
				<td>{{$h -> nama}}</td>
				<td>{{$h -> alamat}}</td>
				<td>{{$h -> kota}}</td>
				<td>{{$h -> provinsi}}</td>
				<td>{{$h -> deskripsi}}</td>
				<td>{{$h -> status}}</td>
			</tr>
			@endforeach
		</tbody>
</table>
</body>
</html>