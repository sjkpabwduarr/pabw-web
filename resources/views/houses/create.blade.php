@extends('layouts.postlogin2')

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<head>
<title>Cari</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style>

* {box-sizing: border-box;}

.pagination li{
      float: left;
      list-style-type: none;
      margin:5px;
    }

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#card{
  background-color: grey;
}
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}
</style>
@section('content')
                    @if (session('status'))                       
                            {{ session('status') }}
                        
                    @endif

                    <body>
                    <center>
                     <div id="Button" style="margin:0 auto;text-align:center">
                     <input type="button" class="btn btn-secondary btn-lg active" aria-pressed="true"  onclick="document.getElementById('Content').style.display='block';Button.style.display='none';" value="Daftarkan Homestay" />
                     </div>
                     <div id="Content" style="display:none;width:300px;padding:10px;margin:0 auto;border:1px solid #aaa;text-align:center;box-shadow: 0px 1px 3px 0px rgba(179,179,179,1);background:#fff">
                     <form enctype="multipart/form-data" method="POST" action="/houses">
                    @csrf   
                        <input required placeholder="Nama Homestay" name="nama" class="form-control">
                        <br>
                        <input placeholder="User id" name="user_id" value="{{ Auth::user()->id }}" readonly=""
                        class="form-control">
                        <br>
                        <input required placeholder="Alamat" name="alamat" class="form-control">
                        <br>  
                        <input required  placeholder="Kota" name="kota" class="form-control">
                        <br>  
                        <input required placeholder="Provinsi" name="provinsi" class="form-control">
                        <br>  
                        <input required placeholder="Deskripsi" name="deskripsi" class="form-control">
                        <br>  
                        <input required placeholder="Status" name="status" class="form-control">
                        <br>
                        <input required placeholder="Harga" name="harga" class="form-control">
                        <br>
                        <input required placeholder="Kamar" name="kamar" class="form-control">
                        <br>
                        <input required type="file" class="form-control" name="images[]" placeholder="address" multiple>						            
                        <button class="btn btn-secondary active" type="submit">Submit</button>
                    
                  </form>  
                     <input type="button" onclick="document.getElementById('Content').style.display='none';Button.style.display='block';" value="&#10006;" />
                     </div> 
                    <script>
                    function fungsiSaya() {
                    var x = document.getElementById("target");
                     if (x.style.display === "none") {
                       x.style.display = "block";
                        } else {
                       x.style.display = "none";
                        }
                        }
                    </script>
                  @if ($homestay!=null)                  
                  <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                            <thead class="thead-dark">
                              <tr >
                                <th class="text-center" >
                                  Nama
                                </th>
                                <th class="text-center">
                                  Alamat
                                </th>
                                <th class="text-center">
                                  Kota
                                </th>
                                  <th class="text-center">
                                  Provinsi
                                </th>
                                  <th class="text-center">
                                  Deskripsi
                                </th>
                                  <th class="text-center">
                                  Status
                                </th>
                                    <th class="text-center" style="border-top: 0px solid #ffffff; border-right: 0px solid #ffffff;">
                                   Action
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr id='addr0' data-id="0" class="hidden">
                                     @foreach($homestay as $p)
                                    <tr>
                                    <td>{{$p -> nama}}</td>
                                    <td>{{$p -> alamat}}</td>
                                    <td>{{$p -> kota}}</td>
                                    <td>{{$p -> provinsi}}</td>
                                    <td>{{$p -> deskripsi}}</td>
                                    <td>{{$p -> status}}</td>
                                    <td>
                                    <a href="{{ url('/projeklaravel/'. $p -> id . '/edit')}}"><button type="button">EDIT</button></a>
                                    <a href="{{ url('/projeklaravel/'. $p -> id . '/delete')}}"><button type="button">DELETE</button></a>
                                    <a href="{{ url('/projeklaravel/'. $p -> id . '/show')}}"><button type="button">LIHAT</button></a></td>
                                    </tr>
                                    @endforeach
                                            </td>
                              </tr>
                            </tbody>                            
                          </table>

                          <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                            <thead class="thead-dark">
                              <tr >
                                <th class="text-center" >
                                  Homestay ID
                                </th>
                                <th class="text-center">
                                  Pelanggan ID
                                </th>
                                <th class="text-center">
                                  Mulai
                                </th>
                                  <th class="text-center">
                                  Selama
                                </th>
                                  <th class="text-center">
                                  Berakhir
                                </th>
                                  <th class="text-center">
                                  Pendapatan
                                </th>
                                 <th class="text-center" style="border-top: 0px solid #ffffff; border-right: 0px solid #ffffff;">
                                   Status
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr id='addr0' data-id="0" class="hidden">
                                    <a hidden> {{$total= 0}} </a>
                                     @foreach($orderan as $o)
                                    <tr>
                                    <td>{{$o -> homestay_id}}</td>
                                    <td>{{$o -> user_id}}</td>
                                    <td>{{$o -> date}}</td>
                                    <td>{{$o -> days}}</td>
                                    <td>{{$o -> finaldate}}</td>
                                    <td>{{$o -> total}}</td>
                                    <td>{{$o -> status}}</td>
                                    @if($o->status=='Finished')
                                    <a hidden> {{$total = $total + $o -> total}} </a>
                                    @endif                                  
                                    </tr>
                                    @endforeach
                                    <td> Total Pendapatan : {{$total}} </td>
                              </tr>
                            </tbody>
                            </table>
                            
                            @endif
                            @if($homestay==null)
                            <h4> Anda Tidak Memiliki Homestay Silahkan Daftarkan Homestay Anda </h4>
                            @endif
                  </center>
                  </body>

@endsection