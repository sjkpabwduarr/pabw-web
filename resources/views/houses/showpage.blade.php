@extends('layouts.postlogin2')
@section('content')

                    @if (session('status'))                       
                            {{ session('status') }}
                        
                    @endif                  
<style>
body {
  font-family: Arial;
  margin: 0;
}

* {
  box-sizing: border-box;
}

img {
  vertical-align: middle;
}

/* Position the image container (needed to position the left and right arrows) */
.container {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}
</style>
<body> 
                    <h1>{{$homestay -> nama}}</h1>
                    Foto Homestay
                    

                    <div class="container">
                    @foreach ($images as $i)
                    <div class="mySlides">
                        <div class="numbertext"></div>
                        <img src="/image/{{ $i }}" style='height: 100%; width: 100%;  height: 500px; width: 950px; object-fit: contain'>
                    </div>
                    @endforeach

                    <a class="prev" onclick="plusSlides(-1)">❮</a>
                    <a class="next" onclick="plusSlides(1)">❯</a>

                    <div class="caption-container">
                    <p id="caption"></p>
                    </div>
                    <?php $int  = 0 ?>
                    <div class="row">                                
                    @foreach ($images as $i)
                    <?php $int = $int + 1?>                   
                    <div class="column">
                    <img class="demo cursor" src="/image/{{$i}}" style='height: 100%; width: 100%;  height: 200px; width: 400px; object-fit: contain' onclick="currentSlide({{$int}})" alt="">
                    </div>                
                    @endforeach
                    </div>
                    </div>
                    <h3>Alamat : {{$homestay -> alamat}}, kota {{$homestay -> kota}}, provinsi {{$homestay -> provinsi}}</h1>
                    <h3>Status : {{$homestay -> status}}</h1>
                    <h3> Kamar : {{$homestay -> kamar}} </h1>
                    <h3>Harga : Rp.{{$homestay -> harga}}</h1>
                    @if($homestay->reviews!=0)
                    <h3>Rating : {{$homestay->rating}} dari {{$homestay->reviews}} reviews</h1>
                    @endif
                    @if($homestay->reviews==0)
                    <h3>Theres No reviews yet</h1>
                    @endif
                    <h3>{{$homestay -> deskripsi}}</h1>
                    
                        <form enctype="multipart/form-data" action="{{('/projeklaravel/'. $homestay -> id . '/order')}}" method="post">
                        @csrf 
                        <div class="form-group">                        
                        <input hidden type="text" class="form-control" name="user_id" value="{{ Auth::user()->id }}" readonly="">
                        </div> 
                        <div class="form-group">
                        <label for="email">Jumlah Hari</label>
                        <input type="text" class="form-control" name="days" >
                        </div>                                      
                        <div class="form-group">
                        <label for="nohp"> Pilih Tanggal :</label>
                        <input required type="date" class="form-control" name="date">
                        </div>                                         
                        <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Pesan</button>                        
                                                                                
                   @if($ulasan != null)
                    @foreach ($ulasan as $u)
                    <div class="card"> 
                                    @if($u->image)                                                                                                                                                
                                    <img style='height: 100%; width: 100%;  height: 200px; width: 300px; object-fit: contain' src="/image/<?php
                                    $images = explode('|',$u->image);                                       
                                    $image = max($images);
                                    echo $image                                  
                                    ?>"/>
                                    @endif                                    
                                     <h3>{{$u->ulasan}}</h3>                                     
                                     <a>By {{$u->user_id}}</a>                                     
                                     </div>
                    @endforeach
                    @endif
                                    <script>
                                    var slideIndex = 1;
                                    showSlides(slideIndex);

                                    function plusSlides(n) {
                                    showSlides(slideIndex += n);
                                    }

                                    function currentSlide(n) {
                                    showSlides(slideIndex = n);
                                    }

                                    function showSlides(n) {
                                    var i;
                                    var slides = document.getElementsByClassName("mySlides");
                                    var dots = document.getElementsByClassName("demo");
                                    var captionText = document.getElementById("caption");
                                    if (n > slides.length) {slideIndex = 1}
                                    if (n < 1) {slideIndex = slides.length}
                                    for (i = 0; i < slides.length; i++) {
                                        slides[i].style.display = "none";
                                    }
                                    for (i = 0; i < dots.length; i++) {
                                        dots[i].className = dots[i].className.replace(" active", "");
                                    }
                                    slides[slideIndex-1].style.display = "block";
                                    dots[slideIndex-1].className += " active";
                                    captionText.innerHTML = dots[slideIndex-1].alt;
                                    }
                                    </script>
@endsection