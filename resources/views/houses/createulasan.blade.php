
@extends('layouts.postlogin2')



<head>
<title>Cari</title>


<style>
* {box-sizing: border-box;}

.pagination li{
      float: left;
      list-style-type: none;
      margin:5px;
    }
.row{
  width: 200px;
}
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}
a { text-decoration : none; color : #000; }
@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}

img {
    max-width: 100%;
    max-height: 100%;
}

.portrait {
    height: 80px;
    width: 30px;
}

.landscape {
    height: 30px;
    width: 80px;
}

.square {
    height: 300px;
    width: 300px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}

</style>
@section('content')                 


@if (session('status'))                       
                            {{ session('status') }}
                        
                    @endif

<body>
@if( $orders->user_id == Auth::user()->id )
    <section class="main-section">
        
        <div class="content">            
            <h1>Ulasan </h1>
            <hr> 
            <form enctype="multipart/form-data" action="{{('/projeklaravel/Myorder/ulas/'. $orders -> id)}}" method="post">
            @csrf
                <div class="form-group">
                    <label for="nama"> Ulasan </label>
                    <input  type="text" class="form-control" name="ulasan">
                </div>
                <div class="form-group">
                        <label for="nohp">Rating 1 - 5</label>
                        <input required type="text" class="form-control" name="rate" >
                </div>           
                <div class="form-group">
                        <label for="nohp">Images :</label>
                        <input type="file" class="form-control" name="images[]" placeholder="address" multiple>
                </div>                                         
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    <button href="projeklaravel/public/create" >Cancel</button> 
                </div>
            </form> 
        </div> 
    </section>
    @endif
</body>

@endsection