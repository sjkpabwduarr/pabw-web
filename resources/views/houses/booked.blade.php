@extends('layouts.postlogin2')
@section('content')

                    @if (session('status'))                       
                            {{ session('status') }}
                        
                    @endif                  
<style>
body {
  font-family: Arial;
  margin: 0;
}

* {
  box-sizing: border-box;
}

img {
  vertical-align: middle;
}

/* Position the image container (needed to position the left and right arrows) */
.container {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}
</style>
<body>                  
                    <h1>Tanggal Sudah Dibooking Orang lain, Pilih Tanggal Lain</h1>
                    <h1> Booked List Table : </h1> 
                    <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                            <thead class="thead-dark">
                              <tr >
                                <th class="text-center" >
                                  Tanggal Mulai
                                </th>
                                <th class="text-center">
                                  Tanggal Selesai
                                </th>
                            </thead>
                            <tbody>
                                <tr id='addr0' data-id="0" class="hidden">
                                     @foreach($homestay as $p)
                                    <tr>
                                    <td>{{$p -> date}}</td>
                                    <td>{{$p -> finaldate}}</td>
                                    </tr>
                                    @endforeach
                                    </td>
                              </tr>
                            </tbody>                            
                          </table>                    
                        <form enctype="multipart/form-data" action="{{('/projeklaravel/'. $id . '/order')}}" method="post">
                        @csrf 
                        <div class="form-group">                        
                        <input hidden type="text" class="form-control" name="user_id" value="{{ Auth::user()->id }}" readonly="">
                        </div> 
                        <div class="form-group">
                        <label for="email">Jumlah Hari</label>
                        <input type="text" class="form-control" name="days" >
                        </div>                                      
                        <div class="form-group">
                        <label for="nohp"> Pilih Tanggal :</label>
                        <input required type="date" class="form-control" name="date">
                        </div>                                         
                        <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Pesan</button>
@endsection