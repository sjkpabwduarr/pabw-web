@extends('layouts.postlogin')


<style>

* {box-sizing: border-box;}

.pagination li{
      float: left;
      list-style-type: none;
      margin:5px;
    }

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#card{
  background-color: grey;
}
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}
</style>
@section('content')
                   

<head>
    <title>Edit Homestay id:{{$homestay -> id}}</title>
</head>
@if (session('status'))                       
                            {{ session('status') }}
                        
                    @endif

<body>
@if( $homestay->user_id == Auth::user()->id )
    <section class="main-section">
        
        <div class="content">            
            <h1>Edit Homestay id:{{$homestay -> id}}</h1>
            <hr> 
            <form enctype="multipart/form-data" action="{{('/projeklaravel/'. $homestay -> id . '/edit')}}" method="post">
            @csrf
                    <div class="form-group">
                    <label for="nama">Nama Homestay:</label>
                    <input type="text" class="form-control" name="nama" value="{{ $homestay -> nama }}">
                </div>

                <div class="form-group">
                    <label for="nama">Alamat:</label>
                    <input type="text" class="form-control" name="alamat" value="{{ $homestay -> alamat }}">
                </div>

                <div class="form-group">
                    <label for="email">Kota:</label>
                    <input type="text" class="form-control" name="kota" value="{{ $homestay -> kota }}">
                </div>
                <div class="form-group">
                    <label for="nohp">Provinsi:</label>
                    <input type="text" class="form-control" name="provinsi" value="{{ $homestay -> provinsi }}">
                </div>
                <div class="form-group">
                    <label for="nohp">Deskripsi:</label>
                    <input type="text" class="form-control" name="deskripsi" value="{{ $homestay -> deskripsi }}">
                </div>
                <div class="form-group">
                    <label for="nohp">Kamar:</label>
                    <input type="text" class="form-control" name="kamar" value="{{ $homestay -> kamar }}">
                </div>
                <div class="form-group">
                    <label for="nohp">Status :</label>
                    <input type="text" name="status" class="form-control" value="{{ $homestay -> status }}">
                </div>
                <div class="form-group">
                    <label for="nohp">Harga :</label>
                    <input type="text" name="harga" class="form-control" value="{{ $homestay -> harga }}">
                </div>                              
                <div class="form-group">
                        <label for="nohp">Images :</label>
                        <input type="file" class="form-control" name="images[]" placeholder="address" multiple>
                </div>                                         
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    <button href="projeklaravel/public/create" >Cancel</button> 
                </div>
            </form> 
        </div> 
    </section>
    @endif
</body>

@endsection