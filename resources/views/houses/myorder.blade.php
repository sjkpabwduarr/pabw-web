@extends('layouts.postlogin2')
@section('content')
@if (session('status'))
                    <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                    </div>
                    @endif                    
                    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
                   
                         <table class="table table-sm " id="tab_logic">
                                <thead class="table-dark">                         
                                <th class="text-center">
                                  Homestay ID
                                </th>
                                <th class="text-center">
                                  Tanggal
                                </th>
                                <th class="text-center">
                                  Lama
                                </th>
                                <th class="text-center">
                                Total Harga 
                                </th>
                                <th class="text-center">
                                  Status
                                </th>                                
                                <th class="text-center">
                                  Action
                                </th>                                
                            </thead>                            
                            <tbody>
                                 <tr id='addr0' data-id="0" class="hidden">
                                    @foreach($order as $p)                                    
                                    <tr>
                                    <td>{{$p -> homestay_id}}</td>
                                    <td>{{$p -> date}}</td>
                                    <td>{{$p -> days}}</td>
                                    <td>{{$p -> total}}</td>                                    
                                    <td>{{$p -> status}}</td>
                                    <td>
                                    <a href="{{ ('/projeklaravel/'. $p -> homestay_id . '/show')}}"><button type="button">LIHAT</button></a>
                                    @if('booking'==$p->status)
                                    <a href="{{ ('/projeklaravel/Myorder/cancel/'.$p -> homestay_id.'')}}"><button type="button">Cancel</button></a>
                                    @endif
                                    @if($p->status == 'Finished' AND $p->ulas == '0')
                                    <a href="{{ ('/projeklaravel/Myorder/jumpulas/'.$p -> id.'')}}"><button type="button">Ulas</button></a>                                    
                                    @endif
                                    </td>
                                    </tr>                                    
                                    @endforeach                                            
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>                                       
                </div>
            
   
@endsection