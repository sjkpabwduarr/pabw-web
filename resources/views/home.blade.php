@extends('layouts.postlogin')


@section('content')
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hommy</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="/css/user.css">
</head>

<body>
    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active" role="presentation"><a href="#">Blog </a></li>
                        <li role="presentation"><a href="#">Shop </a></li>
                        <li role="presentation"><a href="#">Contact me</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="container post">
        <div class="row">
            <div class="col-md-6 post-title">
                <h1>Hommy</h1>
                <p class="author"><strong>Duaarr</strong> <span class="text-muted">17 December 2019 </span></p>
            </div>
            <div class="col-md-6 col-md-offset-0 post-body">
                <p>Merupakan sebuah aplikasi yang memudahkan anda dalam mencari Homestay </p>
                <p>Aplikasi ini beberapa fitur yaitu Mendaftarkan Homestay, Mencari Homestay dan Memilih Homestay</p>
                <figure><img class="img-thumbnail" src="/img/33.jpg">
                    <figcaption> Homestay </figcaption>
                </figure>
                <p>Memiliki banyak partner-partner Homestay yang memilih bergabung terhadap aplikasi Homestay Aja</p>
            </div>
        </div>
    </div>

    <script src="/js/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
 <section id="services">

            <div class="container">
                <div class="row " style="text-align: center;">
                         <div class="col-lg-12 text-center">
                        <h2 class="text-uppercase section-heading">Selamat Datang Para Traveller</h2>
                        <h3 class="text-muted section-subheading">Anda Bingung Mencari Homestay ?</h3>
                        <h4 class="text-muted section-subheading">Tenang Saja Disini Kami Menyediakan Solusinya</h4>
                    
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-stack-1x fa-inverse "></i></span>
                        <h4 class="section-heading">Pesan Homestay </h4>
                        <p class="text-muted">Dengan fitur ini anda bisa memesan Homestay sesuai tempat dan tanggal yang anda inginkan</p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-stack-1x fa-inverse fa-search"></i></span>
                        <h4 class="section-heading">Cari dan Buat Homestay impianmu</h4>
                        <p class="text-muted">Anda dapat mencari Homestay yang anda inginkan, selain itu anda juga dapat mendaftarkan Homestay Anda</p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-stack-1x fa-inverse fa-edit"></i></span>
                        <h4 class="section-heading">Deskripsi dan status Homestay yang selalu Update</h4>
                        <p class="text-muted">Deskripsi Homestay yang lengkap dan jelas, dan Status Homestay yang selalu update jika sudah terisi</p>
                    </div>
                </div>
            </div>
        </div>
        </section>
        
    <footer>
        <h5>Duaarr © 2019</h5></footer>
</html>
@endsection
