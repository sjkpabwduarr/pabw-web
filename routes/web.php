<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/houses','HouseController')->middleware('auth');
Route::get('/homestay','HouseController@indexsearch')->middleware('auth');
Route::get('/homestay/cari','HouseController@cari')->middleware('auth');
Route::get('/projeklaravel/public','HouseController@index')->middleware('auth');
Route::post('/projeklaravel/public','HouseController@store')->middleware('auth');
Route::get('/projeklaravel/public/create','HouseController@homestayku')->middleware('auth');
Route::get('/projeklaravel/{house_id}/edit','HouseController@jumpedit')->middleware('auth');
Route::post('/projeklaravel/{house_id}/edit','HouseController@edit')->middleware('auth');
Route::get('/projeklaravel/{house_id}/delete','HouseController@destroy')->middleware('auth');
Route::get('/projeklaravel/{house_id}/show','HouseController@showpage')->middleware('auth');
Route::post('/projeklaravel/{house_id}/order/','HouseController@order')->middleware('auth');
Route::get('/projeklaravel/Myorder/','HouseController@vieworder')->middleware('auth');
Route::get('/projeklaravel/Myorder/cancel/{house_id}','HouseController@cancelorder')->middleware('auth');
Route::get('/projeklaravel/Myorder/jumpulas/{orders_id}','HouseController@jumpulas')->middleware('auth');
Route::post('/projeklaravel/Myorder/ulas/{orders_id}','HouseController@createulas')->middleware('auth');
//Route::get('/projeklaravel/public/create','HouseController@create')->middleware('auth'); 